//定义一个 class 类
var man = /** @class */ (function () {
    function man() {
    }
    //定义一个 class 类方法 name
    man.prototype.name = function (name) {
        console.log('我是' + name);
    };
    return man;
}());
var lihua = new man();
lihua.name('李华');
