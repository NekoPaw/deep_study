/**
 * 函数定义
 * 函数是一组一起执行一个任务的语句
 * 格式:
 * 
 * function function_name():return_type { 
 *      // 语句
 *      return value; 
 * }
 * 
 * return_type 是返回值的类型，void 不返回任何值 。
 * 
 * return 关键词后跟着要返回的结果。
 * 
 * 一般情况下，一个函数只有一个 return 语句。
 * 
 * 返回值的类型需要与函数定义的返回类型(return_type)一致。
 */
 // 函数定义
 function greet():string { // 返回一个字符串
  return "Hello World"; 
 } 

 function caller() { 
  var msg = greet(); // 调用 greet() 函数 
  console.log(msg); 
 } 

 // 调用函数
 caller();
 
/**
 * 带参函数
 */
 function add(x: number, y: number): number {
  return x + y;
 }
 console.log(add(1,2));

/**
 * 可选参数函数
 */
 function buildName(firstName: string, lastName?: string) {
  if (lastName)
    return firstName + " " + lastName;
  else
    return firstName;
 }
 let result1 = buildName("Bob");
 let result3 = buildName("Bob", "Adams");
 
/**
 * 默认参数函数
 * 设置参数的默认值，在调用函数的时候，如果不传入该参数的值，则使用默认参数
 */
 function calculate_discount(price:number,rate:number = 0.50) { 
  var discount = price * rate; 
  console.log("计算结果: ",discount); 
 } 
 calculate_discount(1000);
 calculate_discount(1000,0.30);

/**
 * 剩余参数函数
 * 在不知道要向函数传入多少个参数的时候，可以使用剩余参数来定义
 */
 function addNumbers(...nums:number[]) {    
  let sum:number = 0; 
  for(let i = 0;i<nums.length;i++) { 
     sum = sum + nums[i]; 
  } 
  console.log("和为：",sum); 
 } 
 addNumbers(1,2,3) 
 addNumbers(10,10,10,10,10)

/**
 * 匿名函数
 * 没有函数名的函数就是匿名函数，除了没有函数名，其他跟标准函数一样
 * 匿名函数可以通过函数表达式赋值给变量
 */
 var msg = function() { 
  return "hello world";  
 } 
 console.log(msg());

/**
 * 匿名函数自调用
 */
 (function () { 
  var x = "Hello!!";   
  console.log(x)     
 })();

/**
 * 构造函数
 * 语法：var res = new Function ([arg1[, arg2[, ...argN]],] functionBody)
 * arg1, arg2, ... argN：参数列表
 * functionBody：一个含有包括函数定义的 JavaScript 语句的 字符串
 */
 var myFunction = new Function("a", "b", "return a * b"); 
 var x = myFunction(4, 3); 
 console.log(x);

/**
 * 递归函数
 * 递归函数即在函数内调用函数本身
 */
 function factorial(number) {
  if (number <= 0) { // 停止执行
      return 1; 
  } else {     
      return (number * factorial(number - 1)); // 调用自身
  } 
 }; 
 console.log(factorial(6)); // 输出 720

/**
 * 箭头函数
 * 语法：( [param1, parma2,…param n] )=>statement;
 */
 var foo = (x:number) => 10 + x;
 console.log(foo(100));