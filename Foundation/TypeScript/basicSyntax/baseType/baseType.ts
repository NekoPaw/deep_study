/**
 * any 类型
 */
 var x : any;
 x = 1;              //number
 console.log(x);

 x = 'I am String!'; //string
 console.log(x);

 x = true;           //boolean
 console.log(x);

/**
 * 在编译时可选择地包含或移除类型检查
 */
 x = 4;
 x.ifItExists();    //ifItExists方法在运行时可能存在，但这里并不会检查
 x.toFixed();

/**
 * 定义存储各种类型数据的数组
 */
 let arrayList: any[] = [1, false, 'fine'];
 arrayList[1] = 100;



/**
 * 可以用 | 来支持多种类型
 */
 var b: number | null | undefined;
 b = 1;             // 运行正确
 b = undefined;     // 运行正确
 b = null;          // 运行正确



/**
 * never 是其它类型（包括 null 和 undefined）的子类型，代表从不会出现的值
 * 函数中它通常表现为抛出异常或无法执行到终止点（例如无限循环）
 */
 let c: never;

 // 运行正确，never 类型可以赋值给 never类型
 c = (()=>{ throw new Error('exception')})();

 // 返回值为 never 的函数可以是抛出异常的情况
 function error(message: string): never {
  throw new Error(message);
 }
 
 // 返回值为 never 的函数可以是无法被执行到的终止点的情况 
 function loop(): never {
  while (true) {}
 }