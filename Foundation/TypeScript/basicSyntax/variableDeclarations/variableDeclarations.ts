/**
 * 变量声明
 * 例子： var sum : number = 1;
 * 可见： 变量声明由  var/let/const + 变量名 + : + 变量类型 + = + 变量的值;  组成
 */
 var sum : number = 1;

 console.log(sum);

/**
 * 类型断言
 * 类型断言可以用来手动指定一个值的类型
 */
 var str = '1';   // 类型推断为 string

 var str2 : number = <number> <any> str;   //str2 是 number 类型, str 是 number 和 any 类型, strr2 是 str 的子集, 将 str 的值和类型赋值给 str2

 console.log(str2);

/**
 * 类型推断
 * 当类型没有给出时，TypeScript 编译器利用类型推断来推断类型
 * 如果由于缺乏声明而不能推断出类型，那么它的类型被视作默认的动态 any 类型
 */
 var num = 1;   // 类型推断为 number
 
 console.log(sum);
 
/**
 * 变量作用域
 * 变量作用域指定了变量定义的位置
 * 程序中变量的可用性由变量作用域决定
 */
 var global_num = 12;         // 全局变量
 class Numbers { 
    num_val = 13;             // 实例变量
    static sval = 10;         // 静态变量
    
    storeNum() : void { 
       var local_num = 14;    // 局部变量
    } 
 } 
 console.log("全局变量为: " + global_num);
 console.log(Numbers.sval);   // 静态变量
 var obj = new Numbers(); 
 console.log("实例变量: " + obj.num_val);

