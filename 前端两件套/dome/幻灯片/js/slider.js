// let sliderItem = document.getElementsByClassName("slider-item"),
//   thumbsItem = document.getElementsByClassName("thumbs-item");

// for (let i = 0; i < thumbsItem.length; i++) {
//   (function (j) {
//     thumbsItem[j].onclick = function () {
//       for (let k = 0; k < thumbsItem.length; k++) {
//         sliderItem[k].className = "slider-item";
//         thumbsItem[k].className = "thumbs-item";
//       }
//       sliderItem[j].className += " active";
//       thumbsItem[j].className += " cur";
//     };
//   })(i);
// }

// 插件化
(function () {
  function Slider(option) {
    this.sliderItem = document.getElementsByClassName(option.sliderItem);
    this.thumbsItem = document.getElementsByClassName(option.thumbsItem);
    this.binkClick();
  }

  Slider.prototype = {
    binkClick: function () {
      let sliders = this.sliderItem,
        thumbs = this.thumbsItem;
      for (let i = 0; i < thumbs.length; i++) {
        (function (j) {
          thumbs[j].onclick = function () {
            for (let k = 0; k < thumbs.length; k++) {
              sliders[k].className = "slider-item";
              thumbs[k].className = "thumbs-item";
            }
            sliders[j].className += " active";
            thumbs[j].className += " cur";
          };
        })(i);
      }
    }
  };

  window.Slider = Slider;
})();
