> 该文件仅描述一些常用的样式知识，想拓展知识面的同学请自行到 W3C 查阅。

## 样式表权重

> 内联样式 > 内部样式表 > 外部样式表

## 选择器写法

### id 选择器

`
#box {

}
`

### 类选择器

`
.box {

}
`

### 属性选择器

`
[id = "box"] {

}
`

### 标签选择器

`
div {

}
`

### 并列选择器

`
.tip.tip-success {

}
`

### 派生选择器(父子选择器)

`
div p {

}
`

### 分组选择器

`
input,
textarea {

}
`

### 通配符选择器

`* {}`

## 选择器权重

> important > id 选择器 > 类选择器 | 属性选择器 | 伪类选择器 > 标签选择器 | 伪元素选择器 > 通配符选择器

## 一些常用的属性

### 字体

> font-family: "Hiragino Sans GB", "Microsoft Yahei", Arial, SimSun,"Helvetica Neue", Helvetica, STHeiTi, sans-serif;

### 字体缩进

> text-indent: 2em;

### 字体修饰(无修饰 | 下划线 | 上划线 | 删除线)

> text-decoration: none | underline | overline | line-through;

### 输入框轮廓高亮

> outline: none;

### 斜体(字体有斜体，将字体变为斜体)

> font-style: italic;

### 斜体(字体无斜体，强制字体变为斜体)

> font-style: oblique;

### 字体粗细(默认 | 粗 | 加粗 | 细)

> font-weight: normal | bold | bolder | lighter

### 边框样式(隐藏 | 圆点 | 虚线 | 实线)

> border-style: hidden | dotted | dashed | solid

### 边框颜色(颜色 | 透明)

> border-color: color | transparent;

### 光标样式(自定义 | 手指 | 禁止 | 问号)

> cursor: url | pointer | not-allowed | help

### 字体换行

> white-space: nowrap;

### 内容溢出隐藏(隐藏 | 滚动条 | 自适应)

> overflow: hidden | scroll | auto;

### 溢出显示省略号(不显示 | 显示)

> text-overflow: clip | ellipsis

### 透明度

> opacity: number; // 0-1
> filter: alpha(opacity=0); // 考虑到兼容性写 0-100

### 内联块元素与内联元素文本对齐(上对齐 | 中对齐 | 底对齐)

> vertical-align: top | middle | bottom | 像素;

### 盒子尺寸边界(注意浏览器兼容)

> box-sizing: border-box | content-box
> -moz-box-sizing: border-box | content-box;
> -webkit-box-sizing: border-box | content-box;
> -o-box-sizing: border-box | content-box;
> -ms-box-sizing: border-box | content-box;
