> 该文件仅描述一些常用的标签知识，想拓展知识面的同学请自行到 W3C 查阅。

## 基础标签(框架基础)

### title 标签

> title 标签主要需要注意的是内容，按以下标准：
>
> 主页：网站名称 + 主要的关键字/关键词的描述
> 详情页：详情名称 + 网站名称 + 简介
> 列表页：分类名称 + 关键字 + 网站名称
> 文章页： 标题 + 分类 + 网站名称

### meta 标签

> meta 标签主要需要注意的是 content 的内容，按以下标准：
>
> keywords 的 content 只能写 100 个字符，编写规则：
> 网站名称 + 分类信息 + 网站名称
>
> description 的 content 填写描述信息，可以填写 80-120 个汉字，规则如下：
> 综合 title 和 keywords 的简单描述

### keywords 内容的比重规则

> 内容越靠前比重越高

### 搜索引擎认知的优先级

> title > description > keywords

### 语言包(lang)

> 通用中文：zh-CN
> 简体中文：zh-Hans zh-CHS
> 繁体中文：zh-Hant zh-CHT
> 香港语言包：zh-HK
> 台湾语言包：zh-TW
> 澳门语言包：zh-MO

### 编码字符集 UTF-8、GB2312、GBK

> UTF-8：万国码，收容了世界上所有国家主流的文字
> GB2312：中国信息处理标准码，用于解析简体中文编码
> GBK：汉字扩展规范，在 GB2312 的基础上，扩大了文字收容范围，增加了繁体字、藏族等少数民族文字

### 关于 <!DOCTYPE html>

> <!DOCTYPE html> 是浏览器兼容模式的声明
>
> 存在 <!DOCTYPE html> 时，浏览器使用的是 W3C 的标准兼容模式(CSS1Compat)
> 不存在 <!DOCTYPE html> 时，浏览器使用的是自身的怪异模式(BackCpmpat)
> 可以通过 js 语法 - document.compatMode 在控制台打印出来

### 常用标签(经常接触)

### h1-h6 标签

> 特性：
> 独占一行(块级元素 block)
> 粗体
> 字体大小不一致
>
> 按倍数继承父元素字体大小(如：2em、1.5em)

### p 标签

> 特性：
> 独占一行(块级元素 block)
> 段落专用标签

### strong 标签

> 特性：
> 不独占一行(内联元素 inline)
> 无法定义宽高
> 语义化标签
> 粗体

### em 标签(emphasize)

> 特性：
> 不独占一行(内联元素 inline)
> 无法定义宽高
> 语义化标签
> 斜体

### i 标签(italic)

> 特性：
> 不独占一行(内联元素 inline)
> 无法定义宽高
> icon 专用标签
> 斜体

### del 标签(delete)

> 特性：
> 不独占一行(内联元素 inline)
> 无法定义宽高
> 语义化标签
> 删除线

### ins 标签(insert)

> 特性：
> 不独占一行(内联元素 inline)
> 无法定义宽高
> 语义化标签
> 下划线

### span 标签

> 特性：
> 不独占一行(内联元素 inline)
> 无法定义宽高
> 语义化标签
> 没有任何样式
> 作用：区分重要文本和高亮文本

### address 标签

> 特性：
> 独占一行(块级元素 block)
> 语义化标签
> 地址专用标签

### div 标签(division)

> 特性：
> 独占一行(块级元素 block)
>
> 用法：
> 网页的结构标签、布局标签

### 文本分隔符

> 在编辑器内，文本的空格和换行都是文本分隔符

### 常用实体字符

> "<"：&lt;
> ">"：&gt;
> "空格"：&nbsp;

### 分割线和换行

> 分割线：<hr />
> 换行： <br />
> 提示：一般在测试时使用，开发过程中严禁使用

### img 标签

> 特性：
> 语义化标签
> 内联块级元素(inline-block)
>
> 属性：
> src: 图片地址
> alt: 加载失败时显示图片的主题(一定要加)
> title: 鼠标移动到图片时显示主题(可加可不加)

### a 标签(anchor)

> 不独占一行(内联元素 inline)
> 作用一：超链接标签
> 属性：
> href: 网址
> target: 页面打开方式(本页面打开/新页面打开)
>
> 作用二：打电话(移动端)
> 属性：
> href: tel:18334007879
>
> 作用三：发邮件(移动端)
> 属性：
> href: mailto:18334007879@163.com
>
> 作用四：锚点定位
> 属性：
> href: #id
>
> 作用五：协议限定符
> 属性：
> href: javascript:void(0)/javascript:;

### 标签嵌套

> 规则：
> 内联元素可以嵌套内联元素
> 块级元素可以嵌套任何元素
> 注意：
> p 标签不能嵌套 div 标签
> a 标签不能嵌套 a 标签

### 注释

> 例：<!--注释-->
>
> 作用:
> 方便维护
> 检查 bug

### 语义化标签的意义

> 增强可读性
> 增强可维护性
> 爬虫程序更青睐语义化标签

## 高级标签

### sup 标签

> 特性：
> 语义化标签
> 不独占一行(内联元素 inline)
>
> 用法：
> 上标标签，如：5° 中的 °
> 写法：5<sup>o<sup>

### sub 标签

> 特性：
> 语义化标签
> 不独占一行(内联元素 inline)
>
> 用法：
> 下标标签，如：SO2 中的 2
> 写法：SO<sub>2<sub>

### ul 标签(unorder list)

> 特性：
> 语义化标签
> 无序列表
> 独占一行(块级元素 block)

### li 标签(list)

> 特性：
> 语义化标签
> 无序列表
> 独占一行(块级元素 block)

## 一些少用但常见的标签

### ol 标签(order list)

> 特性：
> 语义化标签
> 有序列表
> 独占一行(块级元素 block)
>
> 属性：
> type: 序号类型(1,a,A,i,I)
> start: 开始序号
> reversed: 倒序排列

### dl 标签(definition list)

> 特性：
> 语义化标签
> 定义列表
> 独占一行(块级元素 block)

### dt 标签(definition term)

> 特性：
> 语义化标签
> 定义术语
> 独占一行(块级元素 block)

### dd 标签(definition description)

> 特性：
> 语义化标签
> 定义描述
> 独占一行(块级元素 block)

### select 标签

> 特性：
> 语义化标签
> 选项菜单标签
> 内联块级元素(inline-block)
>
> 属性：
> name: key 值

### option 标签

> 特性：
> 语义化标签
> 菜单选项标签
> 属性：
> value: value 值

### table 标签

> 特性：
> 语义化标签
> 表格
> 独占一行(块级元素 block)
>
> 属性：
> border: 边框像素(1,2,3,...)
> cellspace: 单元格间距(1,2,3,...)
> cellpanding: 单元格内边距(1,2,3,...)

### caption 标签

> 特性：
> 语义化标签
> 标题标签
>
> 属性：
> border: 边框像素(1,2,3,...)
> cellspace: 单元格间距(1,2,3,...)
> cellpanding: 单元格内边距(1,2,3,...)

### tr 标签

> 特性：
> 语义化标签
> 表格行标签
>
> 属性：
> border: 边框像素(1,2,3,...)
> cellspace: 单元格间距(1,2,3,...)
> cellpanding: 单元格内边距(1,2,3,...)

### th 标签

> 特性：
> 语义化标签
> 表头标签
>
> 属性：
> align: 文字定位(left,right,center)
> cellspace: 单元格间距(1,2,3,...)
> cellpanding: 单元格内边距(1,2,3,...)

### td 标签

> 特性：
> 语义化标签
> 单元格标签
>
> 属性：
> colspan: 列合并
> rowspan: 行合并
> cellpanding: 单元格内边距(1,2,3,...)

### frameset 标签

> 特性：
> 语义化标签
> 设置框架
> 独占一行(块级元素 block)
>
> 属性：
> cols: 列设置
> rows: 行设置
> 缺点：
> 1、对搜索引擎不友好
> 2、不能存放在 body 里
> 3、不能进行数据交互
> 4、HTTP 请求过多

### frame 标签

> 特性：
> 语义化标签
> 设置框架
> 独占一行(块级元素 block)
>
> 属性：
> name: 绑定 target 的名字
> src: 资源地址

### iframe 标签

> 特性：
> 语义化标签
> 内联框架
> 内联块级元素(inline-block)
>
> 属性：
> name: 绑定 target 的名字
> src: 资源地址
> frameboder：边框(1,2,3,...)
> scrolling：滚动条(yes,no,auto)
>
> 缺点：
> 1、对搜索引擎不友好
> 2、滚动条混乱(两个滚动条)
> 3、外部无法进行加载监控
> 4、数据传递困难

## 表单

### from 表单标签

> 特性：
> 没有样式
> 独占一行(块级元素 block)
>
> 属性：
> methods: 请求方式(get|post)
> action: 请求地址(URL)

### input 标签

> 特性：
> 语义化标签
> 内联块级元素(inline-block)
>
> 属性：
> type: input 类型(text/password/email/reset/submit)
> value: value 值
> name: key 值
> maxlength: 限定最大可输入长度
> id: 与 label 的 for 属性绑定
> readonly：读取属性，不可修改
> disable: 禁止操作，不可修改
> `readonly 与 disable 的区别：设置 disable 属性会导致数据无法提交`

### label 标签

> 特性：
> 语义化标签
> 不独占一行(内联元素 inline)
>
> 属性：
> for: 与 input 的 id 值绑定可以聚焦，以及可以获取 input 的值
> value: value 值
> name: key 值
> maxlength: 限定最大可输入长度

`拓展：
MD5加密小知识：
1、MD5是一种消息摘要的算法
2、不可逆的加密算法
3、只要加密了就不可以解密
4、这种加密方法不需要提供额外的密钥`

### textarea 标签

> 特性：
> 语义化标签
> 内联块级元素(inline-block)
>
> 属性：
> cols: 可见列数(8px \* clos + 17px)
> rows: 可见行数

### fieldset 标签

> 特性：
> 语义化标签
> 独占一行(块级元素 block)

### legend 标签

> 特性：
> 语义化标签
> 独占一行(块级元素 block)
