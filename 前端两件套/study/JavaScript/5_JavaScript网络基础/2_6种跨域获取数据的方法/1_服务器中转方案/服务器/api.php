/*
 * @Author: guanpeijia
 * @Date: 2024-05-08 23:13:26
 * @LastEditors: guanpeijia
 * @LastEditTime: 2024-05-10 20:10:35
 * @Description: file content
 * @FilePath: \前端两件套\study\JavaScript\JavaScript网络基础\6种跨域获取数据的方法\1_服务器中转方案\服务器\api.php
 */

<?php
// 当前文件地址：https://test.play.com/api.php
$url = 'https://test2.play.com/api.php';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
$data = curl_exec($ch);
curl_close($ch);
echo $data;