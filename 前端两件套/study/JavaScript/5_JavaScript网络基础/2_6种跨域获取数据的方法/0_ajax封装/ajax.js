/*
 * @Author: guanpeijia
 * @Date: 2024-05-10 10:54:50
 * @LastEditors: guanpeijia
 * @LastEditTime: 2024-05-10 19:36:03
 * @Description: file content
 * @FilePath: \前端两件套\study\JavaScript\JavaScript网络基础\6种跨域获取数据的方法\0_ajax封装\ajax.js
 */
var $ = (function () {
  var o = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

  if (!o) {
    throw new Error("您的浏览器不支持异步发起HTTP请求");
  }

  function _doAjax(opt) {
    var opt = opt || {},
      type = (opt.type || "GET").toUpperCase(),
      url = opt.url,
      async = opt.async || true,
      data = opt.data || null,
      success = opt.success || function () {},
      error = opt.error || function () {},
      complete = opt.complete || function () {};

    if (!url) {
      throw new Error("您没有填写URL");
    }

    o.open(type, url, async);

    type === "POST" && o.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    o.send(type === "GET" ? null : formatData(data));

    o.onreadystatechange = function () {
      if (o.readystate === 4 && o.status === 200) {
        success(JSON.parse(o.responseText));
      }

      if (o.status === 404) {
        error();
      }

      complete();
    };
  }

  function formatData(obj) {
    var str = "";

    for (const key in obj) {
      str += key + "=" + obj[key] + "&";
    }

    return str.replace(/&$/, "");
  }

  return {
    ajax: function (opt) {
      _doAjax(opt);
    },
    post: function (url, data, callback) {
      _doAjax({
        type: "POST",
        url,
        data,
        callback
      });
    },
    get: function (url, callback) {
      _doAjax({
        type: "GET",
        url,
        callback
      });
    }
  };
})();

// export default $ ;
