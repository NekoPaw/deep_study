/*
 * @Author: guanpeijia
 * @Date: 2024-05-14 21:36:23
 * @LastEditors: guanpeijia
 * @LastEditTime: 2024-05-14 21:54:30
 * @Description: file content
 * @FilePath: \前端两件套\study\JavaScript\8_JavaScript方法封装\utils.js
 */
var commonTools = {
  /*
   * @name: typeof封装
   * @Author: guanpeijia
   * @Date: 2024-05-14 21:36:23
   */
  myTypeof: function (val) {
    var type = typeof val,
      toStr = Object.prototype.toString,
      resSet = {
        "[object Object]": "object",
        "[object Array]": "array",
        "[object Number]": "obj_number",
        "[object String]": "obj_string",
        "[object Boolean]": "obj_boolean",
        "[object Date]": "date",
        "[object RegExp]": "regexp"
      };

    if (val === null) {
      return "null";
    } else if (type === "object") {
      var res = toStr.call(val);
      return resSet[res];
    } else {
      return type;
    }
  },
  /*
   * @name: 深拷贝封装
   * @Author: guanpeijia
   * @Date: 2024-05-14 21:36:23
   */
  deepClone: function(origin, target) {
    let tar = target || {},
      toStr = Object.prototype.toString,
      arrType = "object Array";
    for (const key in origin) {
      if (origin.hasOwnProperty(key)) {
        if (typeof origin[key] === "object" && origin[key] !== null) {
          if (toStr.call(origin[key] === arrType)) {
            tar[key] = [];
          } else {
            tar[key] = {};
          }
          deepClone(origin[key], tar[key]);
        } else {
          tar[key] = origin[key];
        }
      }
    }
    return tar;
  }
};
